# gitlab-raspi

GitLab instance on Raspberry Pi

FROM: raspbian-latest

`sudo apt-get install curl openssh-server ca-certificates apt-transport-https`

`curl https://packages.gitlab.com/gpg.key | sudo apt-key add -`

`sudo apt-get install -y postfix`

`sudo curl -sS https://packages.gitlab.com/install/repositories/gitlab/raspberry-pi2/script.deb.sh | sudo bash`

`sudo EXTERNAL_URL="http://gitlab.example.com" apt-get install gitlab-ce`


https://docs.gitlab.com/omnibus/settings/rpi.html#reduce-running-processes

```
# Reduce the number of running workers to the minimum in order to reduce memory usage
unicorn['worker_processes'] = 2
sidekiq['concurrency'] = 9

# Turn off monitoring to reduce idle cpu and disk usage
prometheus_monitoring['enable'] = false
```

http://www.adamantine.me/2017/10/01/how-to-install-gitlab-on-a-raspberry-pi-2/

```
sidekiq['concurrency'] = 3
unicorn['worker_processes'] = 3
postgresql['shared_buffers'] = "128MB"
postgresql['max_worker_processes'] = 2
nginx['worker_processes'] = 2
```